# pmPlgSaml2

Processmaker plugin that implements authentication through SAML2.

Tested on [Processmaker 3.2.1](https://sourceforge.net/projects/processmaker/files/ProcessMaker/3.2.1/) on linux.

## Functionality

  - SAML2 user login.
  - Auto provisioning and synchronization user data, roles and groups.
  - Multiple IDP configuration on different workspace.
  
## TODO

  - [ ] Support to SLO.
  - [ ] Setup Pages. 

# Requirements.

 - [Composer](https://getcomposer.org/)
 - [PHP YAML Data Serialization](https://secure.php.net/manual/en/book.yaml.php)
 - Git (optional, for download)

# Installation.

Paths and names:

 - **{pm_path}**: ProcessMaker instalation path. (usually: /opt/processmaker).
 - **{pm_plugins_path}**: ProcessMaker plugin path. (usually: {pm_path}/workflow/engine/plugins)
 - **{workspace_name}**: Workspace name. (usually: workflow)
 - **{pm_workspace_path}**: Workspace share data path. (usually: {pm_path}/shared/{workflow_name})
 - **{plugins_path}**: Path to download pmPlgSaml2. (sample: /plugins)
 - **{idp}**: Identity provider name.

## Download.

### From Git.

```
cd {plugins_path}
git clone https://gitlab.com/rodriguezrjrr/pmPlgSaml2.git
```

### Pack.

```
cd {plugins_path}
wget https://gitlab.com/rodriguezrjrr/pmPlgSaml2/-/archive/master/pmPlgSaml2-master.tar.gz
tar -xzf pmPlgSaml2-master.tar.gz
mv pmPlgSaml2-master pmPlgSaml2
```

### Download dependencies.

```
cd {plugins_path}/pmPlgSaml2
php composer install
```

## Install plugin on ProcessMaker.

Make synbolic links to ProcessMaker plugin path.

```
ln -s {plugins_path}/pmPlgSaml2/pmPlgSaml2.php {pm_plugins_path}/
ln -s {plugins_path}/pmPlgSaml2/pmPlgSaml2 {pm_plugins_path}/
```

Or copy the content

```
cp {plugins_path}/pmPlgSaml2/pmPlgSaml2.php {pm_plugins_path}/
cp -r {plugins_path}/pmPlgSaml2/pmPlgSaml2 {pm_plugins_path}/
```

# Configuration.

The configuration files are read from the **pm_workspace_path**. For each workspace
there must be different configuration files.

Create o copy files to **pm_workspace_path** and edit them.

```
cp {plugins_path}/pmPlgSaml2/saml.yaml.sample {pm_workspace_path}/saml.yaml
cp {plugins_path}/pmPlgSaml2/saml_default.yaml.sample {pm_workspace_path}/saml_{ipd}.yaml
```

 - `{pm_workflow_path}/saml.yaml`: General config file.

```
create_user_data: true               # Auto provisioning user.
default_status: ACTIVE               # Default status if idp not provisioned.
default_due_date: +1 year            # Default due date if idp not provisioned. see: [formats.php](https://secure.php.net/manual/en/datetime.formats.php)
default_role: PROCESSMAKER_OPERATOR  # Default role if idp not provisioned.

sync_user_data: true      # Sync user data on login.
sync_user_role: true      # Sync rol of user on login.
sync_user_groups: true    # Sync user group on login.
update_auth_src: false    # Change auth source on login if is different.  

validate_source = true    # Only accept login if auth source match.
```

 - `{pm_workflow_path}/saml_{idp}.yaml`: Idp configuration.

```
# IDP info data
idp_id: pmplgdev
sso_url: https://localhost:9443/samlsso
name_id_format: urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress

# bindings types
http_bindig: urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect
#http_bindig: urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST

# Sing, validate and accept assertion encryption
sign_authn_request: true
validate_ipd_sign: true
assertion_encryption: true
sing_algorithm: http://www.w3.org/2000/09/xmldsig#rsa-sha1
digest_algorithm: http://www.w3.org/2000/09/xmldsig#sha1

certificate: |-
   -----BEGIN CERTIFICATE-----
   ..............................
   -----END CERTIFICATE-----
private_key: |-
   -----BEGIN CERTIFICATE-----
   ..............................
   -----END CERTIFICATE-----
idp_certificate: |-
   -----BEGIN CERTIFICATE-----
   ..............................
   -----END CERTIFICATE-----

# Map user data info
# Sample map default wso2 attributes.
attr_map:
   USR_LASTNAME:   http://wso2.org/claims/lastname
   USR_FIRSTNAME:  http://wso2.org/claims/givenname
   USR_EMAIL:      http://wso2.org/claims/emailaddress
   USR_ADDRESS:    http://wso2.org/claims/addresses
   USR_ZIP_CODE:   http://wso2.org/claims/postalcode
   USR_COUNTRY:    http://wso2.org/claims/country
   USR_LOCATION:   http://wso2.org/claims/addresses.locality
   USR_PHONE:      http://wso2.org/claims/identity/phoneVerified
   USR_STATUS:     http://wso2.org/claims/active
   USR_ROLE:       http://wso2.org/claims/role

# Map status values
# remote_value: local_value
status_value_map:
   active: ACTIVE
   inactive: INACTIVE
   inactive: VACATION
```

# Management.

On Admin > Plugins page you can activate and deactivate the plugin called pmPlgSaml2.

![Admin_plugin](img/admin_plugin.png)

# Uninstallation.

Before uninstalling, please deactive the plugin in Admin > Plugins page.

Remove links whith

```
rm {pm_plugins_path}/pmPlgSaml2.php
ln {pm_plugins_path}/pmPlgSaml2/pmPlgSaml2
```

## Re-assign auth source for users.
TODO
