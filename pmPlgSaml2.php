<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

G::LoadClass("plugin");

if(!defined('DIR_SEP')) {
    define('DIR_SEP', DIRECTORY_SEPARATOR);
}

class pmPlgSaml2Plugin extends PMPlugin
{
    const NAME = 'pmPlgSaml2';
    
    // Path constants
    const PATH = PATH_PLUGINS . self::NAME;
    const PATH_VENDOR = self::PATH . DIR_SEP . 'vendor';
    const PATH_MODEL = self::PATH . DIR_SEP . 'model';
    const PATH_INTERFACE = self::PATH . DIR_SEP . 'interface';
    const PATH_FACTORY = self::PATH . DIR_SEP . 'factory';
    const PATH_CLASSES = self::PATH . DIR_SEP . 'classes';
    
    // Path to make links on processmaker soruce.
    // [[$target, $link]]
    private static  $link_files = [
        [
            self::PATH_CLASSES . DIR_SEP . 'class.saml2.php',
            PATH_RBAC . 'plugins' . DIR_SEP . 'class.saml2.php'
        ]
    ];


    public function pmPlgSaml2Plugin($sNamespace, $sFilename = null)
    {
        $res = parent::PMPlugin($sNamespace, $sFilename);
        $this->sFriendlyName = 'SAML2 Plugin';
        $this->sDescription = 'SAML2 Processmaker plugin ';
        $this->sPluginFolder = self::NAME;
        $this->sSetupPage = false;
        $this->iVersion = '0.1';
        
        return $res;
    }
    
    public function setup()
    {
        if (defined('PM_SINGLE_SIGN_ON')
            && isset($_GET['login'])
            && $_GET['login'] == 'saml'
        ) {
            $this->registerTrigger(PM_SINGLE_SIGN_ON, 'singleSignOn');
        }
    }
    
    public function install()
    {
        
    }
    
    public function enable()
    {
        // Make sybolic link on processmaker source
        foreach (self::$link_files as $link) {
            if (file_exists($link[1])) {
                unlink($link[1]);
            }
            symlink($link[0], $link[1]);
        }
    }
    
    public function disable()
    {
        // Remove sybolic link
        foreach (self::$link_files as $link) {
            if (file_exists($link[1])) {
                unlink($link[1]);
            }
        }
    }
    
    public function uninstall()
    {
        
    }
        
}

$oPluginRegistry =& PMPluginRegistry::getSingleton();
$oPluginRegistry->registerPlugin(pmPlgSaml2Plugin::NAME, __FILE__);
?>