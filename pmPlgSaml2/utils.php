<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

if (!function_exists('to_camel_case')) {
    
    /**
     * Conver strint to camelCaseFormat
     * @param string $str
     * @param boolean $uper true for UperCamelCase
     * @return string
     */
    function to_camel_case($str, $uper=false) {
        if ($str && is_string($str)) {
            if ($uper) {
                $str[0] = strtoupper($str[0]);
            }
            return preg_replace_callback(
                '/_([a-z])/',
                function ($c) {
                    return strtoupper($c[1]);
                },
                $str
            );
        }
        return '';
    }
}

?>