<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SMAL Object Interface
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
interface PmPlgSaml2Interface
{
    
    const RESPONSE = 1;
    const LOGOUTRESPONSE = 2;
    
    /**
     * Constructor
     * @param PmPlgSaml2IdpCfg $conf config injection
     */
    function __construct(PmPlgSaml2IdpCfg $conf);
    
    /**
     * Build Authn Request
     */
    function makeAuthnRequest();
    
    /**
     * Build Responce
     */
    function makeResponse();
    
    /**
     * Binding request
     */
    function bindRequest();
    
    /**
     * Get user data from Assertion
     * @return array User array data
     */
    function getResponceUserData();
    
}