<?php

/*
 This file is part of pmPlgSaml2.

 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * IDP config Model
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
final class PmPlgSaml2IdpCfg extends PmPlgSaml2Model
{

    private $attr_map = [];
    private $status_value_map = null;
    private $certificate = null;
    private $digest_algorithm = null;
    private $http_bindig = null;
    private $idp_certificate = null;
    private $idp_id = null;
    private $logout_url = null;
    private $name_id_format = null;
    private $private_key = null;
    private $sing_algorithm = null;
    private $sign_authn_request = null;
    private $sign_logout_request = null;
    private $sso_url = null;
    private $validate_ipd_sign = null;
    private $assertion_encryption = null;
    private $class = 'LigthSAML';
    private $class_file = null;

    public function __construct($data)
    {
        parent::__construct($data);
        if (!$this->class_file) {
            $this->class_file = implode(DIR_SEP, [
                pmPlgSaml2Plugin::PATH_CLASSES,
                'ligthSAML',
                'LigthSAML.php'
            ]);
        }
    }

    /**
     *
     * @return array
     */
    public function getAttrMap()
    {
        return $this->attr_map;
    }

    public function getStatusValueMap()
    {
        return $this->status_value_map;
    }

    /**
     * @return mixed
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @return mixed
     */
    public function getDigestAlgorithm()
    {
        return $this->digest_algorithm;
    }

    /**
     * @return mixed
     */
    public function getHttpBindig()
    {
        return $this->http_bindig;
    }

    /**
     * @return mixed
     */
    public function getIdpCertificate()
    {
        return $this->idp_certificate;
    }

    /**
     * @return mixed
     */
    public function getIdpId()
    {
        return $this->idp_id;
    }

    /**
     * @return mixed
     */
    public function getLogoutUrl()
    {
        return $this->logout_url;
    }

    /**
     * @return mixed
     */
    public function getNameIdFormat()
    {
        return $this->name_id_format;
    }

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->private_key;
    }

    /**
     * @return mixed
     */
    public function getSingAlgorithm()
    {
        return $this->sing_algorithm;
    }

    /**
     * @return bool
     */
    public function getSignAuthnRequest()
    {
        return $this->sign_authn_request;
    }

    /**
     * @return bool
     */
    public function getSignLogoutRequest()
    {
        return $this->sign_logout_request;
    }

    /**
     * @return mixed
     */
    public function getSsoUrl()
    {
        return $this->sso_url;
    }

    /**
     * @return bool
     */
    public function getValidateIpdSign()
    {
        return $this->validate_ipd_sign;
    }

    /**
     * @return bool
     */
    public function getAssertionEncryption()
    {
        return $this->assertion_encryption;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getClassFile()
    {
        return $this->class_file;
    }

    /**
     *
     * @param array $attr_map
     * @return PmPlgSaml2IdpCfg
     */
    public function setAttrMap($attr_map)
    {
        if (!is_array($attr_map)) {
            throw new Exception('Invalid AttrMap');
        }
        $attr_local = UserAttr::all();
        foreach ($attr_map as $key => $val) {
            if (in_array($key, $attr_local)) {
                $this->attr_map[$key] = filter_var($val, FILTER_SANITIZE_URL);
            }
        }
        return $this;
    }

    public function setStatusValueMap($status_value_map)
    {
        if (is_array($status_value_map)
            && in_array('ACTIVE', $status_value_map)
            && in_array('INACTIVE', $status_value_map)
            && in_array('VACATION', $status_value_map)
            ) {
                $this->sync_user_groups = boolval($status_value_map);
            }
            return $this;
    }

    /**
     * @param mixed $certificate
     */
    public function setCertificate($certificate)
    {
        $this->certificate = filter_var($certificate, FILTER_SANITIZE_STRING);
        return $this;
    }

    /**
     * @param mixed $digest_algorithm
     */
    public function setDigestAlgorithm($digest_algorithm)
    {
        $this->digest_algorithm = filter_var($digest_algorithm, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $http_bindig
     */
    public function setHttpBindig($http_bindig)
    {
        $this->http_bindig = filter_var($http_bindig, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $idp_certificate
     */
    public function setIdpCertificate($idp_certificate)
    {
        $this->idp_certificate = filter_var($idp_certificate, FILTER_SANITIZE_STRING);
        return $this;
    }

    /**
     * @param mixed $idp_id
     */
    public function setIdpId($idp_id)
    {
        $this->idp_id = filter_var($idp_id, FILTER_SANITIZE_STRING);
        return $this;
    }

    /**
     * @param mixed $logout_url
     */
    public function setLogoutUrl($logout_url)
    {
        $this->logout_url = filter_var($logout_url, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $name_id_format
     */
    public function setNameIdFormat($name_id_format)
    {
        $this->name_id_format = filter_var($name_id_format, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $private_key
     */
    public function setPrivateKey($private_key)
    {
        $this->private_key = filter_var($private_key, FILTER_SANITIZE_STRING);
        return $this;
    }

    /**
     * @param mixed $sing_algorithm
     */
    public function setSingAlgorithm($sing_algorithm)
    {
        $this->sing_algorithm = filter_var($sing_algorithm, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $sign_authn_request
     */
    public function setSignAuthnRequest($sign_authn_request)
    {
        $this->sign_authn_request = boolval($sign_authn_request);
        return $this;
    }

    /**
     * @param mixed $signLogoutRequest
     */
    public function setSignLogoutRequest($sign_logout_request)
    {
        $this->sign_logout_request = boolval($sign_logout_request);
        return $this;
    }

    /**
     * @param mixed $sso_url
     */
    public function setSsoUrl($sso_url)
    {
        $this->sso_url = filter_var($sso_url, FILTER_SANITIZE_URL);
        return $this;
    }

    /**
     * @param mixed $validate_ipd_sign
     */
    public function setValidateIpdSign($validate_ipd_sign)
    {
        $this->validate_ipd_sign = boolval($validate_ipd_sign);
        return $this;
    }

    /**
     * @param mixed $validate_ipd_sign
     */
    public function setAssertionEncryption($assertion_encryption)
    {
        $this->assertion_encryption = boolval($assertion_encryption);
        return $this;
    }

    /**
     * @parm string $class class name implement of PmPlgSaml2Interface
     */
    public function setClass($class)
    {
        $this->class = filter_var($class, FILTER_SANITIZE_STRING);
        return $this;
    }

    /**
     * @parm string $classFile file whit class implement PmPlgSaml2Interface
     */
    public function setClassFile($classFile)
    {
        $this->class = filter_var($classFile, FILTER_SANITIZE_STRING);
        return $this;
    }

}
