<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * User attributes constans
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
final class UserAttr
{
    
    const FIRSTNAME = 'USR_FIRSTNAME';
    const LASTNAME = 'USR_LASTNAME';
    const USERNAME = 'USR_USERNAME';
    const EMAIL = 'USR_EMAIL';
    const ADDRESS = 'USR_ADDRESS';
    const ZIP_CODE = 'USR_ZIP_CODE';
    const COUNTRY = 'USR_COUNTRY';
    const CITY = 'USR_CITY';
    const LOCATION = 'USR_LOCATION';
    const PHONE = 'USR_PHONE';
    const POSITION = 'USR_POSITION';
    const STATUS = 'USR_STATUS';
    const DUE_DATE = 'USR_DUE_DATE';
    const ROLE = 'USR_ROLE';
    const GROUPS = 'USR_GROUPS';
    
    private static $reflectionClass;
    
    /**
     * Get all user attributes name in array
     * @return string |boolean
     */
    static function all() {
        if(self::$reflectionClass == null) {
            self::$reflectionClass = new ReflectionClass(__CLASS__);
            return self::$reflectionClass->getConstants();
        }
        return false;
    }
}