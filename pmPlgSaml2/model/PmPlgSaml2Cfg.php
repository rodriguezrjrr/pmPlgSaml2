<?php

/*
 This file is part of pmPlgSaml2.

 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * General Config model
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
final class PmPlgSaml2Cfg extends PmPlgSaml2Model
{
    private $default_role = 'PROCESSMAKER_OPERATOR';
    private $default_status = 'INACTIVE';
    private $default_due_date = '+1 year';
    private $sync_user_data = true;
    private $sync_user_role = true;
    private $sync_user_groups = true;
    private $update_auth_src = true;
    private $create_user_data = true;
    private $validate_source = true;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    public function getDefaultRole()
    {
        return $this->default_role;
    }

    public function getDefaultStatus()
    {
        return $this->default_status;
    }

    public function getDefaultDueDate()
    {
        return $this->default_due_date;
    }

    public function getSyncUserData()
    {
        return $this->sync_user_data;
    }

    public function getSyncUserRole()
    {
        return $this->sync_user_role;
    }

    public function getSyncUserGroups()
    {
        return $this->sync_user_groups;
    }

    public function getUpdateAuthSrc()
    {
        return $this->update_auth_src;
    }

    public function getCreateUserData()
    {
        return $this->create_user_data;
    }

    public function getValidateSource()
    {
        return $this->validate_source;
    }

    public function setDefaultRole($default_role)
    {
        $this->default_role = filter_var($default_role, FILTER_SANITIZE_STRING);
        return $this;
    }

    public function setDefaultStatus($default_status)
    {
        $this->default_status = filter_var($default_status, FILTER_SANITIZE_STRING);
        return $this;
    }

    public function setDefaultDueDate($default_due_date)
    {
        $this->default_due_date = filter_var($default_due_date, FILTER_SANITIZE_STRING);
        return $this;
    }

    public function setSyncUserData($sync_user_data)
    {
        $this->sync_user_data = boolval($sync_user_data);
        return $this;
    }

    public function setSyncUserRole($sync_user_role)
    {
        $this->sync_user_role = boolval($sync_user_role);
        return $this;
    }

    public function setSyncUserGroups($sync_user_groups)
    {
        $this->sync_user_groups = boolval($sync_user_groups);
        return $this;
    }

    public function setUpdateAuthSrc($update_auth_src)
    {
        $this->update_auth_src = boolval($update_auth_src);
        return $this;
    }

    public function setCreateUserData($create_user_data)
    {
        $this->create_user_data = boolval($create_user_data);
        return $this;
    }

    public function setValidateSource($validate_source)
    {
        $this->validate_source = boolval($validate_source);
        return $this;
    }

}
