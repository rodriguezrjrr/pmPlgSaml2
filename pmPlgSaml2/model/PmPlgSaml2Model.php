<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Base model class
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
abstract class PmPlgSaml2Model
{
    
    /**
     * Constructor
     * @param array $data init data
     */
    public function __construct($data)
    {
        if (is_array($data)) {
            $reflex = new ReflectionClass($this);
            foreach (array_keys($data) as $attr) {
                $method_name = 'set' . to_camel_case($attr, true);
                if ($reflex->hasMethod($method_name)){
                    $reflex
                    ->getMethod($method_name)
                    ->invoke($this, $data[$attr]);
                }
            }
        }
    }
    
}