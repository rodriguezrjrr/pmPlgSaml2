<?php

/*
 This file is part of pmPlgSaml2.

 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SAML Object Factory
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
final class PmPlgSaml2Factory
{
    private static $_instance = null;

    public static function getSingleton()
    {
        if (self::$_instance === null) {
            self::$_instance = new PmPlgSaml2Factory();
        }

        return self::$_instance;
    }

    private $__saml_intance = [];
    private $__saml_class_reflex = [];

    /**
     *
     * @param PmPlgSaml2IdpCfg $cfg
     * @return PmPlgSaml2Interface
     */
    public function makeSAML(PmPlgSaml2IdpCfg $cfg) {
        $oid = spl_object_hash ($cfg);
        if (!isset($this->__saml_intance[$oid])) {
            if(!isset($this->__saml_intance[$cfg->getClass()])) {
                if (!class_exists($cfg->getClass())) {
                    if (!file_exists($cfg->getClassFile())) {
                        throw new Exception('File ' . $cfg->getClassFile() . ' not exist.');
                    }
                    require_once $cfg->getClassFile();
                }
                if (!class_exists($cfg->getClass())) {
                    throw new Exception('Class ' . $cfg->getClass() . ' not exist.');
                }
                $reflex = new ReflectionClass($cfg->getClass());
                if (!$reflex->implementsInterface(PmPlgSaml2Interface::class)) {
                    throw new Exception('Class ' . $cfg->getClass() . ' not implement ' . PmPlgSaml2Interface::class . '.');
                }
                $this->__saml_class_reflex[$cfg->getClass()] = $reflex;
            }
            $this->__saml_intance[$oid] = $this->__saml_class_reflex[$cfg->getClass()]->newInstance($cfg);
        }
        return $this->__saml_intance[$oid];
    }
}
