<?php

/*
 This file is part of pmPlgSaml2.

 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

require pmPlgSaml2Plugin::PATH_VENDOR . DIR_SEP . 'autoload.php';

// Load scripts
$scripts = [
    pmPlgSaml2Plugin::PATH . DIR_SEP . 'utils.php',
    pmPlgSaml2Plugin::PATH_MODEL . DIR_SEP . 'PmPlgSaml2Model.php',
    pmPlgSaml2Plugin::PATH_MODEL . DIR_SEP . 'PmPlgSaml2IdpCfg.php',
    pmPlgSaml2Plugin::PATH_MODEL . DIR_SEP . 'PmPlgSaml2Cfg.php',
    pmPlgSaml2Plugin::PATH_MODEL . DIR_SEP . 'UserAttr.php',
    pmPlgSaml2Plugin::PATH_INTERFACE . DIR_SEP . 'PmPlgSaml2Interface.php',
    pmPlgSaml2Plugin::PATH_FACTORY . DIR_SEP . 'PmPlgSaml2Factory.php',
];
foreach ($scripts as $script) {
    require_once $script;
}

class pmPlgSaml2Class extends PMPlugin {

    /**
     * Idp config
     * @var PmPlgSaml2IdpCfg
     */
    private $__idp_cfg = null;

    /**
     * Auth source data
     * @var array
     */
    private $__auth_src = null;

    /**
     * Roles IDS
     * @var array
     */
    private $__roles = null;

    /**
     * Grous IDS
     * @var array
     */
    private $__groups = null;

    /**
     * Global cgf
     * @var PmPlgSaml2Cfg
     */
    private $__cfg = null;


    public function __construct() {
        set_include_path(
            PATH_PLUGINS . 'pmPlgSaml2' . PATH_SEPARATOR . get_include_path()
        );
    }

    public function singleSignOn()
    {
        // Define IDP id
        $pre_idp = false;
        if (isset($_GET['idp'])) {
            $pre_idp = filter_var($_GET['idp'], FILTER_SANITIZE_STRING);
        }
        $valid_get_idp = (
            $pre_idp
            && preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $_GET['idp'])
        );
        $idp = $valid_get_idp ? $pre_idp : 'default';

        // Load configs
        $this->__loadCfg();
        $this->__loadIdpCfg($idp);

        // Get SAML Object
        $saml = PmPlgSaml2Factory::getSingleton()->makeSAML($this->__idp_cfg);

        if (isset($_POST['SAMLResponse']) || isset($_GET['SAMLResponse'])){
            switch ($saml->makeResponse()) {
                case PmPlgSaml2Interface::RESPONSE:
                    return $this->__login($saml, $idp);
                    break;
                case PmPlgSaml2Interface::LOGOUTRESPONSE:
                    # TODO: Implementar LOGOUTRESPONSE
                    break;
            }
            
        } elseif (isset($_POST['SAMLRequest']) || isset($_GET['SAMLRequest'])) {
            # TODO: Logout request.
        } else {
            // Bind Auth request
            $saml->makeAuthnRequest();
            $saml->bindRequest();
        }
    }

    /**
     * Load IPD config from file yaml in workspace path
     * @param string $idp
     * @throws Exception
     */
    private function __loadIdpCfg($idp)
    {
        $file_path = PATH_DATA_SITE . 'saml_' . $idp .'.yaml';
        $data = yaml_parse_file($file_path);
        if (!$data) {
            $error = error_get_last();
            throw new Exception($error['message']);
        }

        $this->__idp_cfg = new PmPlgSaml2IdpCfg($data);
    }

    /**
     * Get Auth sources from database to $this->__auth_src
     * @param string $idp
     */
    private function __loadIdpAuthSrc($idp)
    {
        global  $RBAC;
        $RBAC =& RBAC::getSingleton();
        $authSourcesObj =& $RBAC->authSourcesObj;
        $src_name = 'saml2_' . $idp;

        $criteria = $authSourcesObj->getAllAuthSources();
        $objects = AuthenticationSourcePeer::doSelectRS($criteria);
        $objects->setFetchmode(ResultSet::FETCHMODE_ASSOC);
        while ($objects->next()) {
            $data = $objects->getRow();
            if ($data['AUTH_SOURCE_NAME'] == $src_name
                && $data['AUTH_SOURCE_PROVIDER'] == 'saml2'
            ) {
                $this->__auth_src = $data;
                return;
            }
        }

        $data = [
            'AUTH_SOURCE_NAME' => $src_name,
            'AUTH_SOURCE_PROVIDER' => 'saml2',
        ];
        $data['AUTH_SOURCE_UID'] = $authSourcesObj->create($data);
        $this->__auth_src = $data;
    }

    /**
     * Load General config from saml.yaml in workspace path$uid
     * @throws Exception
     */
    private function __loadCfg()
    {
        $file_path = PATH_DATA_SITE . 'saml.yaml';
        $data = [];
        if (file_exists($file_path)) {
            $data = yaml_parse_file($file_path);
            if (!$data) {
                $error = error_get_last();
                throw new Exception($error['message']);
            }
        }

        $this->__cfg = new PmPlgSaml2Cfg($data);
    }

    private function __getRcabUData($data)
    {
        if($data && is_array($data) && isset($data[UserAttr::USERNAME])) {
            global  $RBAC;
            $RBAC =& RBAC::getSingleton();
            $userobj =& $RBAC->userObj;
            return $userobj->getByUsername($data[UserAttr::USERNAME]);
        }
        return false;
    }

    private function __getWfUData($data)
    {
        if($data && is_array($data) && isset($data['USR_UID'])) {
            $user_repo = new \ProcessMaker\BusinessModel\User();
            return $user_repo->getUser($data['USR_UID']);
        }
        return false;
    }

    private function __createUser($data)
    {
        $user_repo = new \ProcessMaker\BusinessModel\User();

        if (!isset($data[UserAttr::DUE_DATE])) {
            $time = strtotime($this->__cfg->getDefaultDueDate());
            $newdate = date('Y-m-d', $time);
            $data[UserAttr::DUE_DATE] = $newdate;
        }

        if (!isset($data[UserAttr::STATUS])) {
            $data[UserAttr::STATUS] = $this->__cfg->getDefaultStatus();
        }

        $data['USR_NEW_PASS'] = substr(md5(time().random_int(0,99)), 0, 20);

        if (!isset($data[UserAttr::ROLE])) {
            $data[UserAttr::ROLE] = $this->__cfg->getDefaultRole();
        }

        $new_data = $user_repo->create($data);
        $rbac_user_data = $this->__getRcabUData($new_data);
        $this->__updateUserAuthSource($rbac_user_data);
        return $new_data;
    }

    /**
     * Update user info
     * @param array $data
     * @return array
     */
    private function __updateUser($new_data, $wf_data)
    {
        if($new_data && $wf_data) {
            $user_repo = new \ProcessMaker\BusinessModel\User();
            foreach ($new_data as $key => $val) {
                if ($key != 'GROUPS_VERIFY' && $wf_data[$key] != $val) {
                    $wf_data[$key] = $val;
                }
            }
            return $user_repo->update(
                $wf_data['USR_UID'],
                $wf_data,
                '00000000000000000000000000000001'
            );
        }

    }

    /**
     * Update auth source of user
     * @param string $sUsrUid user uid
     */
    private function __updateUserAuthSource($data)
    {
        $auth_data = $this->__auth_src;

        if (!$this->verifyAuthSource($data)) {
            $data['UID_AUTH_SOURCE'] = $auth_data['AUTH_SOURCE_UID'];
            $data['USR_AUTH_TYPE'] = $auth_data['AUTH_SOURCE_PROVIDER'];
            global  $RBAC;
            $RBAC =& RBAC::getSingleton();
            $RBAC->userObj->update($data);
        }
    }

    private function verifyAuthSource($data)
    {
        $auth_data = $this->__auth_src;
        return (
            $data['UID_AUTH_SOURCE'] == $auth_data['AUTH_SOURCE_UID']
            && $data['USR_AUTH_TYPE'] == $auth_data['AUTH_SOURCE_PROVIDER']
        );
    }

    /**
     * Perform login
     * @param array $data user data
     * @return boolean
     */
    private function __login(PmPlgSaml2Interface $saml, $idp)
    {
        
        $user_data = $saml->getResponceUserData();
        global  $RBAC;
        $RBAC =& RBAC::getSingleton();
        $RBAC->initRBAC();
        $this->__loadIdpAuthSrc($idp);
        $this->__normalizeData($user_data);
        $rbac_user_data = $this->__getRcabUData($user_data);
        $wf_user_data = $this->__getWfUData($rbac_user_data);
        $new = false;
        if (!($rbac_user_data || $wf_user_data )
            && $this->__cfg->getCreateUserData())
        {
            $wf_user_data = $this->__createUser($user_data);
            # esto se esta repitiendo
            $rbac_user_data = $this->__getRcabUData($user_data);
            $new = true;
            if ($this->__cfg->getSyncUserGroups()) {
                $this->__updateGroups($user_data, $wf_user_data['USR_UID']);
            }
        }
        if (
            !$new
            && $wf_user_data
            && $rbac_user_data
            && $this->__cfg->getSyncUserData()
        ) {
            if (
                !$this->__cfg->getSyncUserRole()
                && isset($user_data[UserAttr::ROLE])
                ) {
                    unset($user_data[UserAttr::ROLE]);
                }
                $wf_user_data = $this->__updateUser($user_data, $wf_user_data);
                if($this->__cfg->getUpdateAuthSrc()) {
                    $this->__updateUserAuthSource($rbac_user_data);
                }
                if ($this->__cfg->getSyncUserGroups()) {
                    $this->__updateGroups($user_data, $wf_user_data['USR_UID']);
                }
        }

        $login = $RBAC->verifyUser($rbac_user_data['USR_USERNAME']);
        if (
            $login
            && (!$this->__cfg->getValidateSource()
                || $this->verifyAuthSource($rbac_user_data))
        ) {
            $RBAC->singleSignOn = true;
            $_SESSION['USER_LOGGED']  = $rbac_user_data['USR_UID'];
            $_SESSION['USR_USERNAME'] = $rbac_user_data['USR_USERNAME'];

            // Remove alerts
            if (isset($_SESSION['G_MESSAGE'])) {
                unset($_SESSION['G_MESSAGE']);
            }
            if (isset($_SESSION['G_MESSAGE_TYPE'])) {
                unset($_SESSION['G_MESSAGE_TYPE']);
            }
            $pkmauth = PATH_METHODS . 'login' . PATH_SEP . 'authentication.php';
            require_once $pkmauth;
            die();
        }

        return false;
    }

    private function __normalizeData(&$data)
    {
        foreach ($data as $key => $val) {
            switch ($key) {
                case UserAttr::FIRSTNAME:
                case UserAttr::LASTNAME:
                case UserAttr::USERNAME:
                case UserAttr::EMAIL:
                case UserAttr::ADDRESS:
                case UserAttr::ZIP_CODE:
                case UserAttr::COUNTRY:
                case UserAttr::CITY:
                case UserAttr::LOCATION:
                case UserAttr::PHONE:
                case UserAttr::POSITION:
                case UserAttr::STATUS:
                case UserAttr::DUE_DATE:
                    if(is_array($val)) {
                        $data[$key] = count($val) > 0 ? $val[0] : null;
                    }
                    break;
                case UserAttr::ROLE:
                    if (!$this->__cfg->getSyncUserRole()) {
                        break;
                    }
                    $data[$key] = null;
                    if(!is_array($val)) {
                        $new_val = [];
                        $new_val[] = $val;
                        $val = $new_val;
                    }
                    foreach ($val as $role_code) {
                        if (
                            $data[$key] == null
                            && in_array($role_code, $this->__getRoles())
                        ) {
                            $data[$key] = $role_code;
                        }
                        if ($role_code != $data[$key]) {
                            $gid = $this->__groupId($role_code);
                            if ($gid) {
                                if(!isset($data['GROUPS_VERIFY'])) {
                                    $data['GROUPS_VERIFY'] = [];
                                }
                                if(!in_array($gid, $data['GROUPS_VERIFY'])) {
                                    $data['GROUPS_VERIFY'][] = $gid;
                                }
                            }
                        }
                    }
                    if ($data[$key] == null) {
                        $data[$key] = $this->__cfg->getDefaultRole();
                    }
                    break;
                case UserAttr::GROUPS:
                    foreach ($val as $group_name) {
                        $gid = $this->__groupId($group_name);
                        if ($gid) {
                            if(!isset($data['GROUPS_VERIFY'])) {
                                $data['GROUPS_VERIFY'] = [];
                            }
                            if(!in_array($gid, $data['GROUPS_VERIFY'])) {
                                $data['GROUPS_VERIFY'][] = $gid;
                            }

                        }
                    }
                    break;
            }
        }
    }

    /**
     * Get all roles id on array
     * @return array
     */
    private function __getRoles()
    {
        if ($this->__roles == null) {
            $this->__roles = [];
            global  $RBAC;
            $RBAC =& RBAC::getSingleton();
            $sys_roles = $RBAC->getAllRoles();
            foreach ($sys_roles as $sys_role) {
                if ($sys_role['ROL_STATUS']) {
                    $this->__roles[] = $sys_role['ROL_CODE'];
                }
            }
        }
        return $this->__roles;
    }

    /**
     * Get groups groups data
     * @return array
     */
    private function __getGroups()
    {
        if ($this->__groups == null) {
            $this->__groups = [];
            $groups = new Groupwf();
            $data = $groups->getAll();
            $this->__groups = $data->data;
        }
        return $this->__groups;
    }

    /**
     * Get group id by name
     * @param string $name
     * @return string | boolean
     */
    private function __groupId($name)
    {
        foreach ($this->__getGroups() as $group) {
            if ($group['CON_VALUE'] == $name) {
                return $group['GRP_UID'];
            }
        }
        return false;
    }

    /**
     * Sync user groups
     * @param array $data user data
     * @param string $uid user id
     */
    private function __updateGroups($data, $uid) {

        if (!isset($data['GROUPS_VERIFY'])
            || count($data['GROUPS_VERIFY']) < 1) {
            return;
        }

        $avai_groups = null;
        $assg_groups = null;
        if (!class_exists('Groups')) {
            G::LoadClass("groups");
        }
        $group_repo = new Groups();

        $criteria = $group_repo->getAvailableGroupsCriteria($uid, '');
        $objects = GroupwfPeer::doSelectRS($criteria);
        $objects->setFetchmode(ResultSet::FETCHMODE_ASSOC);
        $avai_groups = [];
        while ($objects->next()) {
            $avai_groups[] = $objects->getRow()['GRP_UID'];
        }

        $criteria = $group_repo->getAssignedGroupsCriteria($uid, '');
        $objects = GroupwfPeer::doSelectRS($criteria);
        $objects->setFetchmode(ResultSet::FETCHMODE_ASSOC);
        $assg_groups = [];
        while ($objects->next()) {
            $assg_groups[] = $objects->getRow()['GRP_UID'];
        }


        foreach ($data['GROUPS_VERIFY'] as $gid) {
            $in_avai = in_array($gid, $avai_groups);
            $in_assg = in_array($gid, $assg_groups);
            if (!$in_assg && $in_avai) {
                $group_repo->addUserToGroup($gid, $uid);
            }
        }

        foreach ($assg_groups as $gid) {
            if (!in_array($gid, $data['GROUPS_VERIFY'])) {
                $group_repo->removeUserOfGroup($gid, $uid);
            }
        }
    }
}

?>
