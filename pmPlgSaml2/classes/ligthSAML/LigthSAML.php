<?php

/*
 This file is part of pmPlgSaml2.

 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

use LightSaml\Model\Protocol\AuthnRequest;
use LightSaml\Helper;
use LightSaml\Model\Assertion\Issuer;
use LightSaml\Model\Protocol\NameIDPolicy;
use LightSaml\Credential\X509Certificate;
use LightSaml\Credential\KeyHelper;
use LightSaml\Model\XmlDSig\SignatureWriter;
use LightSaml\Binding\BindingFactory;
use LightSaml\Context\Profile\MessageContext;
use LightSaml\SamlConstants;
use Symfony\Component\HttpFoundation\Request;
use LightSaml\Model\Context\DeserializationContext;
use LightSaml\Credential\X509Credential;

/**
 * Implemntation of SAML Interface with LigthSAML(https://www.lightsaml.com/)
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
class LigthSAML implements PmPlgSaml2Interface
{

    private $__cfg = null;

    private $__request = null;

    /**
     *
     * @var LightSaml\Model\Protocol\Response|\LightSaml\Model\Protocol\LogoutResponse
     */
    private $__response = null;


    /**
     * {@inheritDoc}
     * @see PmPlgSaml2Interface::__construct()
     */
    public function __construct(PmPlgSaml2IdpCfg $conf)
    {
        $this->__cfg = $conf;
    }

    /**
     * {@inheritDoc}
     * @see PmPlgSaml2Interface::bindRequest()
     */
    public function bindRequest()
    {

        if (!$this->__request) {
            throw new Exception("Request not created");
        }

        $bindingFactory = new BindingFactory();
        $messageContext = new MessageContext();
        $messageContext->setMessage($this->__request);
        $bind = $bindingFactory->create($this->__cfg->getHttpBindig());
        $httpResponse = $bind->send($messageContext);

        switch ($this->__cfg->getHttpBindig()) {
            case SamlConstants::BINDING_SAML2_HTTP_POST:
                echo $httpResponse->getContent();
                break;
            case SamlConstants::BINDING_SAML2_HTTP_REDIRECT:
                header('Location: ' . $httpResponse->getTargetUrl());
                break;
        }
        die();
    }

    /**
     * {@inheritDoc}
     * @see PmPlgSaml2Interface::getResponceUserData()
     */
    public function getResponceUserData()
    {
        $asset = $this->__getAssertion();
        $attrStatement = $asset->getFirstAttributeStatement();

        $data = [];
        foreach ($this->__cfg->getAttrMap() as $local_attr => $saml_attr) {
            $attr = $attrStatement->getFirstAttributeByName($saml_attr);
            if ($attr) {
                $data[$local_attr] = $attr->getAllAttributeValues();
            }
        }

        $data[UserAttr::USERNAME] = $asset
        ->getSubject()
        ->getNameID()
        ->getValue();

        $s_map = $this->__cfg->getStatusValueMap();
        if ($s_map
            && isset($data[UserAttr::STATUS])
            && isset($s_map[$data[UserAttr::STATUS]])
        ) {
            $data[UserAttr::STATUS] = $s_map[$data[UserAttr::STATUS]];
        }

        return $data;
    }

    /**
     * {@inheritDoc}
     * @see PmPlgSaml2Interface::makeAuthnRequest()
     */
    public function makeAuthnRequest()
    {
        $this->__request = new AuthnRequest();
        $name_policy = new NameIDPolicy();
        $scheme = $_SERVER['REQUEST_SCHEME'];
        $host = $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['REQUEST_URI'];
        $service_url = $scheme . '://' . $host . $uri;
        $name_policy->setFormat($this->__cfg->getNameIdFormat());

        $this->__request
        ->setAssertionConsumerServiceURL($service_url)
        ->setProtocolBinding($this->__cfg->getHttpBindig())
        ->setID(Helper::generateID())
        ->setIssueInstant(new \DateTime())
        ->setDestination($this->__cfg->getSsoUrl())
        ->setIssuer(new Issuer($this->__cfg->getIdpId()))
        ->setNameIDPolicy($name_policy);

        if($this->__cfg->getSignAuthnRequest()) {
            $this->__signRequest();
        }
    }

    /**
     * {@inheritDoc}
     * @see PmPlgSaml2Interface::makeResponse()
     */
    public function makeResponse()
    {
        $request = Request::createFromGlobals();

        $bindingFactory = new BindingFactory();
        $binding = $bindingFactory->getBindingByRequest($request);
        $messageContext = new MessageContext();
        $binding->receive($request, $messageContext);
        $responce = $messageContext->asResponse();

        if($this->__cfg->getValidateIpdSign()) {
            $sign = $responce->getSignature();
            if(null == $sign) {
                throw new Exception("No existe.");
            }
            $certificate = new X509Certificate();
            $certificate->loadPem($this->__cfg->getIdpCertificate());
            $key = KeyHelper::createPublicKey($certificate);
            try {
                $ok = $sign->validate($key);
                if (!$ok) {
                    throw new Exception("No valida");
                }
            } catch (\Exception $ex) {
                throw new Exception("No valida2", null, $ex);
            }
        }

        $for_data = [
            PmPlgSaml2Interface::RESPONSE => 'asResponse',
            PmPlgSaml2Interface::LOGOUTRESPONSE => 'LogoutResponse'
        ];

        foreach ($for_data as $ret => $method) {
            $res = call_user_func([$messageContext, $method]);
            if ($res) {
                $this->__response = $res;
                return  $ret;
            }
        }

        return null;
    }

    private function __signRequest()
    {
        $certificate = new X509Certificate();
        $certificate->loadPem($this->__cfg->getCertificate());
        $private_key = KeyHelper::createPrivateKey(
            $this->__cfg->getPrivateKey(),
            '',
            false,
            $this->__cfg->getSingAlgorithm()
            );
        $sign = new SignatureWriter($certificate, $private_key);
        $sign->setDigestAlgorithm($this->__cfg->getDigestAlgorithm());
        $this->__request->setSignature($sign);
    }

    /**
     *
     * @return \LightSaml\Model\Assertion\Assertion
     */
    private function __getAssertion()
    {
        if($this->__cfg->getAssertionEncryption()) {
            $certificate = new X509Certificate();
            $certificate->loadPem($this->__cfg->getCertificate());
            $private_key = KeyHelper::createPrivateKey(
                $this->__cfg->getPrivateKey(),
                '',
                false,
                $this->__cfg->getSingAlgorithm()
                );
            $credential = new X509Credential($certificate, $private_key);
            $decryptDeserializeContext = new DeserializationContext();
            $reader = $this->__response
            ->getFirstEncryptedAssertion();
            $assertion = $reader->decryptMultiAssertion(
                [$credential],
                $decryptDeserializeContext
                );
            return $assertion;
        }
        return $this->__response->getFirstAssertion();
    }
}
