<?php

/*
 This file is part of pmPlgSaml2.
 
 pmPlgSaml2 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 pmPlgSaml2 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Las to RBAC Auth Source
 * @author @author Rómulo Rodríguez <rodriguezrjrr@gmail.com>
 *
 */
class SAML2
{
    
    public function VerifyLogin($strUser, $strPass)
    {
        // Preven login with password
        // TODO: Posiblemente se pueda redirigir a una pagina indicando que se
        //       debe atuntenticar por SAML.
        return -2;
    }
    
}